const config = require('config');

module.exports.loop = function () {
    memory_cleanup();
    run_creeps();
    run_towers();
    spawn_creeps();
}

/**
 * Perform memory cleanup
 */
function memory_cleanup() {
    for (var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
            console.log('Clearing non-existing creep memory:', name);
        }
    }
}

/**
 * Run creep AI
 */
function run_creeps() {
    const roleHarvester = require('role.harvester');
    const roleUpgrader = require('role.upgrader');
    const roleBuilder = require('role.builder');
    
    for (var name in Game.creeps) {
        var creep = Game.creeps[name];
        if (creep.memory.role == 'harvester') {
            roleHarvester.run(creep);
        }
        if (creep.memory.role == 'upgrader') {
            roleUpgrader.run(creep);
        }
        if (creep.memory.role == 'builder') {
            roleBuilder.run(creep);
        }
    }
}

/**
 * Run tower AI
 */
function run_towers() {
    for (let i in Game.rooms) {
        let towers = Game.rooms[i].find(FIND_MY_STRUCTURES,
            {filter: {structureType: STRUCTURE_TOWER}});
        let targets = Game.rooms[i].find(FIND_HOSTILE_CREEPS);
        for (let j in towers) {
            for (let k in targets) {
                towers[j].attack(targets[k]);
            }
        }
    }
}

/**
 * Determine if creeps are needed and spawn them
 */
function spawn_creeps() {
    var to_spawn = [];
    for (const [key, value] of Object.entries(Memory.desired_creeps)) {
        var current_creeps = _.filter(Game.creeps, (creep) =>
            creep.memory.role == key);
        if (current_creeps.length < Memory.desired_creeps[key]) {
            to_spawn.push(key);
        }
    }

    const level = `level${Memory.level}`;

    for (var i in Game.spawns) {
        if (to_spawn.length == 0) break;
        if (Game.spawns[i].spawning) continue;
        let c = to_spawn.pop();
        var newName = c + Game.time;
        var body = config.builds[c][level].body;
        if (Game.spawns[i].spawnCreep(body, newName,
            {memory: {role: c}}) != 0) {
            to_spawn.push(c);
        }
        /*
        else {
            let spawningCreep = Game.creeps[Game.spawns[i].spawning.name];
            Game.spawns[i].room.visual.text(
                '🛠️' + spawningCreep.memory.role,
                spawn.pos.x + 1, spawn.pos.y,
                {align: 'left', opacity: 0.8});
        }
        */
    }
}
