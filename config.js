var config = {
    builds: {
        builder: {
            level1: {
                body: [MOVE, CARRY, WORK],
                cost: 200,
            },
            level2: {
                body: [CARRY, CARRY, MOVE, WORK, WORK],
                cost: 400,
            },
            level3: {
                body: [CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, WORK, WORK, WORK,
                    WORK],
                cost: 750,
            },
        },
        harvester: {
            level1: {
                body: [MOVE, CARRY, WORK],
                cost: 200,
            },
            level2: {
                body: [CARRY, CARRY, MOVE, WORK, WORK],
                cost: 400,
            },
            level3: {
                body: [CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, WORK, WORK, WORK,
                    WORK],
                cost: 750,
            },
        },
        upgrader: {
            level1: {
                body: [MOVE, CARRY, WORK],
                cost: 200,
            },
            level2: {
                body: [CARRY, CARRY, MOVE, WORK, WORK],
                cost: 400,
            },
            level3: {
                body: [CARRY, CARRY, CARRY, CARRY, MOVE, MOVE, WORK, WORK, WORK,
                    WORK],
                cost: 750,
            },
        },
    },
};

module.exports = config;
