var find = require('find');

var roleHarvester = {

    /**
     *  @param {Creep} creep
     */
    run: function(creep) {
        if (creep.memory.job == undefined) {
            if (creep.store.getUsedCapacity() > 0) {
                var success = find.find_storage(creep);
                if (!success) {
                    success = find.find_source(creep, RESOURCE_ENERGY);
                }
            }
            else {
                var success = find.find_source(creep, RESOURCE_ENERGY);
            }
        }
        else if (creep.memory.job.type == 'harvest') {
            var pos = new RoomPosition(creep.memory.job.target.x,
                creep.memory.job.target.y,
                creep.memory.job.target.roomName);
            var target = pos.lookFor(LOOK_SOURCES);
            target = target[0];

            if (target.energy == 0) {
                var success = find.find_source(creep);
            }
            else if (creep.store.getFreeCapacity() == 0) {
                var success = find.find_storage(creep, RESOURCE_ENERGY);
            }
            else if (creep.harvest(target) == ERR_NOT_IN_RANGE) {
                var success = true;
                if (creep.moveTo(target, {
                    visualizePathStyle: {stroke: '#ffffff'}}) == ERR_NO_PATH) {
                    success = find.find_source(creep);
                }
                else {
                    var success = true;
                }
            }
            else {
                var success = true;
            }
            if (!success && creep.store.getUsedCapacity() > 0) {
                success = find.find_storage(creep, RESOURCE_ENERGY);
            }
        }
        else if (creep.memory.job.type == 'store') {
            if (creep.store.getUsedCapacity() == 0) {
                var success = find.find_source(creep);
            }
            else {
                var pos = new RoomPosition(creep.memory.job.target.x,
                    creep.memory.job.target.y,
                    creep.memory.job.target.roomName);
                var target = pos.lookFor(LOOK_STRUCTURES);
                target = target[0];
                if (target.store.getFreeCapacity(RESOURCE_ENERGY) == 0) {
                    var success = find.find_storage(creep, RESOURCE_ENERGY);
                    if (!success && creep.store.getFreeCapacity() > 0) {
                        success = find.find_source(creep);
                    }
                }
                else {
                    let r = creep.transfer(target, RESOURCE_ENERGY);
                    if (r == ERR_NOT_IN_RANGE) {
                        var success = true;
                        if (creep.moveTo(target,
                            {visualizePathStyle: {stroke: '#ffffff'}}) ==
                            ERR_NO_PATH) {
                            success = false;
                        }
                    }
                    else if (r == ERR_FULL) {
                        var success = find.find_storage(creep, RESOURCE_ENERGY);
                    }
                    else {
                        var success = true;
                    }
                }
                if (!success && creep.store.getFreeCapacity() > 0) {
                    var success = find.find_source(creep);
                }
            }
        }
        else {
            if (creep.store.getUsedCapacity() > 0) {
                var success = find.find_storage(creep);
                if (!success) {
                    success = find.find_source(creep, RESOURCE_ENERGY);
                }
            }
            else {
                var success = find.find_source(creep, RESOURCE_ENERGY);
            }
        }
        if (success == false) {
            /* Only randomly move 1/4 of the time */
            if (Math.floor(Math.random() * 4) == 3) {
                creep.move(Math.floor(Math.random() * 8) + 1);
            }
        }
	}
};

module.exports = roleHarvester;
