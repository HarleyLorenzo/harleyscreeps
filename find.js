var find = {

    /**
    * @param {Creep} creep
    * @returns {Boolean} True if found a valid target to build
    */
    find_build: function (creep) {
        let targets = creep.room.find(FIND_CONSTRUCTION_SITES);
        /**
         * Sort that calculates the approximate time to build including
         * movement
         */
        targets.sort((a,b) => {
            /* If the work needed for both is above what this creep can do,
             * simply prefer the one which is closer to being done */
            if (((a.progressTotal - a.progress) >
                creep.store.getUsedCapacity()) &&
                ((b.progressTotal - b.progress) >
                creep.store.getUsedCapacity())) {
                return (a.progressTotal - a.progress) -
                    (b.progressTotal - b.progress);
            }
            /* If the work needed for one is above what the creep can do,
             * prefer the closest one */
            else if (((a.progressTotal - a.progress) >
                creep.store.getUsedCapacity()) ||
                ((b.progressTotal - b.progress) >
                creep.store.getUsedCapacity())) {
                return ((Math.abs(creep.pos.x - a.pos.x) +
                    Math.abs(creep.pos.y - a.pos.y)) -
                    (Math.abs(creep.pos.x - b.pos.x) +
                    Math.abs(creep.pos.y - b.pos.y)));
            }
            var works = 0;
            for (let i in creep.body) {
                if (creep.body[i].type == WORK) {
                    works++;
                }
            }
            var time = Math.ceil(((a.progressTotal - a.progress)/(works*10))) -
                Math.ceil(((b.progressTotal - b.progress)/(works*10)));
            time += ((Math.abs(creep.pos.x - a.pos.x) +
                Math.abs(creep.pos.y - a.pos.y)) -
                (Math.abs(creep.pos.x - b.pos.x) +
                Math.abs(creep.pos.y - b.pos.y)));
            return time;
        });

        for (let i in targets) {
            if (creep.build(targets[i]) == ERR_NOT_IN_RANGE) {
                if (creep.moveTo(targets[i], {visualizePathStyle:
                    {stroke: '#ffffff'}}) != ERR_NO_PATH) {
                    creep.memory.job = {'type': 'build',
                        'target': targets[i].pos};
                    return true;
                }
            }
        }
        creep.memory.job = {'type:': null};
        return false;
    },

    /**
    * @param {Creep} creep
    * @returns {Boolean} True if found a valid target to repair
    */
    find_repair: function (creep) {
        var targets = creep.room.find(FIND_STRUCTURES, {
            filter: object => object.hits < object.hitsMax
        });
        /**
         * Sort that calculates the approximate time to repair including
         * movement
         */
        targets.sort((a,b) => {
            /* If the work needed for both is above what this creep can do,
             * simply prefer the one with less hp */
            if (((a.hitsMax - a.hits)/100 > creep.store.getUsedCapacity()) &&
                ((b.hitsMax - b.hits)/100 > creep.store.getUsedCapacity())) {
                return a.hits - b.hits;
            }
            /* If the work needed for one is above what the creep can do,
             * prefer the closest one */
            else if (((a.hitsMax - a.hits)/100 >
                creep.store.getUsedCapacity()) ||
                ((b.hitsMax - b.hits)/100 >
                creep.store.getUsedCapacity())) {
                return ((Math.abs(creep.pos.x - a.pos.x) +
                    Math.abs(creep.pos.y - a.pos.y)) -
                    (Math.abs(creep.pos.x - b.pos.x) +
                    Math.abs(creep.pos.y - b.pos.y)));
            }
            var works = 0;
            for (let i in creep.body) {
                if (creep.body[i].type == WORK) {
                    works++;
                }
            }
            var time = Math.ceil(((a.hitsMax - a.hits)/(works*100))) -
                Math.ceil(((b.hitsMax - b.hits)/(works*100)));
            time += ((Math.abs(creep.pos.x - a.pos.x) +
                Math.abs(creep.pos.y - a.pos.y)) -
                (Math.abs(creep.pos.x - b.pos.x) +
                Math.abs(creep.pos.y - b.pos.y)));
            return time;
        });

        for (let i in targets) {
            if (creep.repair(targets[i]) == ERR_NOT_IN_RANGE) {
                if (creep.moveTo(targets[i], {visualizePathStyle:
                    {stroke: '#ffffff'}}) != ERR_NO_PATH) {
                    creep.memory.job = {'type': 'repair',
                        'target': targets[i].pos};
                    return true;
                }
            }
        }
        creep.memory.job = {'type:': null};
        return false;
    },

    /**
    * @param {Creep} creep
    * @returns {Boolean} True if found a valid source
    */
    find_source: function (creep) {
        var targets = creep.room.find(FIND_SOURCES, {
            filter: (source) => {
                return (source.energy > 0);
            }
        });
        for (let i in targets) {
            if (creep.harvest(targets[i]) == ERR_NOT_IN_RANGE) {
                if (creep.moveTo(targets[i], {
                    visualizePathStyle: {stroke: '#ffffff'}}) != ERR_NO_PATH) {
                    creep.memory.job = {'type': 'harvest',
                        'resource': RESOURCE_ENERGY,
                        'target': targets[i].pos};
                    return true;
                }
            }
        }
        creep.memory.job = {'type': null};
        return false;
    },

    /**
    * @param {Creep} creep
    * @param {String} resource - resource constant
    * @returns {Boolean} True if found a valid storage
    */
    find_storage: function (creep, resource) {
        var targets = creep.room.find(FIND_STRUCTURES, {
            filter: (structure) => {
                if (structure.store == undefined) return false;
                return (structure.store.getFreeCapacity(resource) > 0);
            }
        });

        for (let i in targets) {
            let r = creep.transfer(targets[i], resource);
            if (r == ERR_NOT_IN_RANGE) {
                if (creep.moveTo(targets[i],
                    {visualizePathStyle: {stroke: '#ffffff'}}) !=
                    ERR_NO_PATH) {
                    creep.memory.job = {'type': 'store',
                        'resource': resource,
                        'target': targets[i].pos};
                    return true;
                }
            }
            else if (r == ERR_FULL) {
                continue;
            }
        }
        creep.memory.job = {'type': null};
        return false;
    }
}

module.exports = find;
